﻿
# Globale Variablen

$SharePath = '\\fauad.fau.de\RRZE\Projekte\Entwicklung Integration Verfahren\Ressourcenverfahren\Verfahren'
$InputPath = ""
$DestinationSwitch = 0
$SwitchSwitch = 0

# Start / Reset

function Begin($SwitchSwitch){
	$Error.Clear()

	if ($SwitchSwitch -eq 0){

		$InputPath = ""
		$DestinationSwitch = 0
		$SwitchSwitch = 0
		cls
		$InputPath = Read-Host -Prompt 'Bitte Pfad der Word-Dokumente eingeben'
		checkPath(1)
	} else{
		DestPath
	}
}

# Prüfen ob Ziel/Ursprungs Pfad existiert/Verfügbar ist

function checkPath($InputDestSwitch){

	if ($InputDestSwitch -eq 1){
		if (!$InputPath){
			$InputPath = $PSScriptRoot
		} 
		$pathexist = Test-Path -Path $InputPath
	} elseif(!$DestinationPath){
		$DestinationPath = $PSScriptRoot
		$pathexist = Test-Path -Path $DestinationPath
	}

	if ($pathexist -eq $false) {
		echo "Der Pfad wurde nicht gefunden!"
		Start-Sleep -Seconds 1
		Begin(0)
	} elseif( $DestinationSwitch -eq 1) {
		RV-Verfahren 
	} elseif( $DestinationSwitch -eq 2) {
		OwnPath
	} else{
		Begin(1)
	}
}

# Vorgefertigte Zielepfäde

function RV-Verfahren{
	$Error.Clear()
	$DestinationPath = $SharePath
	$word_app = New-Object -ComObject Word.Application

	Get-ChildItem -Path $InputPath -Filter *.doc? | ForEach-Object {
		$document = $word_app.Documents.Open($_.FullName)

		$Directory = $_.DirectoryName
		$purePDFName = $_.BaseName

		
		$pdf_filename = "$($Directory)\$($purePDFName).pdf"


		$document.SaveAs([ref] $pdf_filename, [ref] 17)

		$document.Close()

		#### File to correct path ####

		if ( $purePDFName -like '*Bay*' -and $purePDFName -like '*DVS*') {

			Move-Item -Path $pdf_filename -Destination $SharePath\BayDVS
			if ( $Error.count -gt 0 ) {
                Start-Sleep -Seconds 5
				Remove-Item -Path $pdf_filename 
				$Error.Clear()
			} 

		} elseif ( $purePDFName -like '*Bib*' -and $purePDFName -like '*Split*') {
			Move-Item -Path $pdf_filename -Destination $SharePath\BibSplit
			if ( $Error.count -gt 0 ) {
                Start-Sleep -Seconds 2
				Remove-Item -Path $pdf_filename 
				$Error.Clear()
			} 

		} elseif ( $purePDFName -like '*CEUS*' ) {
			Move-Item -Path $pdf_filename -Destination $SharePath\CEUS
			if ( $Error.count -gt 0 ) {
                Start-Sleep -Seconds 5
				Remove-Item -Path $pdf_filename 
				$Error.Clear()
			} 

		} elseif ( $purePDFName -like '*Citrix*' -and $purePDFName -like '*Detect*' ) {
			Move-Item -Path $pdf_filename -Destination $SharePath\CitrixDetect
			if ( $Error.count -gt 0 ) {
                Start-Sleep -Seconds 2
				Remove-Item -Path $pdf_filename 
				$Error.Clear()
			} 
		} elseif ( $purePDFName -like '*Citrix*' -and $purePDFName -like '*Infrastruktur*' ) {
			Move-Item -Path $pdf_filename -Destination $SharePath\Citrix-Infrastruktur
			if ( $Error.count -gt 0 ) {
                Start-Sleep -Seconds 5
				Remove-Item -Path $pdf_filename 
				$Error.Clear()
			}
		} elseif ( $purePDFName -like '*F1*' -and $purePDFName -like '*Aktenverwaltung*' ) {
			Move-Item -Path $pdf_filename -Destination $SharePath\F1-Aktenverwaltung
			if ( $Error.count -gt 0 ) {
                Start-Sleep -Seconds 5
				Remove-Item -Path $pdf_filename 
				$Error.Clear()
			} 
		} elseif ( $purePDFName -like '*FAMOS*' ) {
			Move-Item -Path $pdf_filename -Destination $SharePath\FAMOS
			if ( $Error.count -gt 0 ) {
                Start-Sleep -Seconds 5
				Remove-Item -Path $pdf_filename 
				$Error.Clear()
			} 
		} elseif ( $purePDFName -like '*fau*' -and $purePDFName -like '*org*' ) {
			Move-Item -Path $pdf_filename -Destination $SharePath\FAU.org
			if ( $Error.count -gt 0 ) {
                Start-Sleep -Seconds 5
				Remove-Item -Path $pdf_filename 
				$Error.Clear()
			} 
		} elseif ( $purePDFName -like '*fau*' -and $purePDFName -like '*STAMM*' ) {
			Move-Item -Path $pdf_filename -Destination $SharePath\FAU_STAMM
			if ( $Error.count -gt 0 ) {
                Start-Sleep -Seconds 5
				Remove-Item -Path $pdf_filename 
				$Error.Clear()
			}
		} elseif ( $purePDFName -like '*HFIN*' -and $purePDFName -like '*Fins*' ) {
			Move-Item -Path $pdf_filename -Destination $SharePath\HFIN-Fins
			if ( $Error.count -gt 0 ) {
                Start-Sleep -Seconds 5
				Remove-Item -Path $pdf_filename 
				$Error.Clear()
			} 
		} elseif ( $purePDFName -like '*HIS*' -and $purePDFName -like '*ADM*' ) {
			Move-Item -Path $pdf_filename -Destination $SharePath\HIS-ADM
			if ( $Error.count -gt 0 ) {
                Start-Sleep -Seconds 5
				Remove-Item -Path $pdf_filename 
				$Error.Clear()
			} 
		} elseif ( $purePDFName -like '*HIS*' -and $purePDFName -like '*COB*' ) {
			Move-Item -Path $pdf_filename -Destination $SharePath\HIS-COB
			if ( $Error.count -gt 0 ) {
                Start-Sleep -Seconds 5
				Remove-Item -Path $pdf_filename 
				$Error.Clear()
			} 
		} elseif ( $purePDFName -like '*HIS*' -and $purePDFName -like '*FSV*' -and $purePDFName -like '*IVS*') {
			Move-Item -Path $pdf_filename -Destination $SharePath\HIS-FSV-IVS
			if ( $Error.count -gt 0 ) {
                Start-Sleep -Seconds 5
				Remove-Item -Path $pdf_filename 
				$Error.Clear()
			} 
		} elseif ( $purePDFName -like '*HIS*' -and $purePDFName -like '*FSV*' -and $purePDFName -like '*MBS*') {
			Move-Item -Path $pdf_filename -Destination $SharePath\HIS-FSV-MBS
			if ( $Error.count -gt 0 ) {
                Start-Sleep -Seconds 5
				Remove-Item -Path $pdf_filename 
				$Error.Clear()
			}
		} elseif ( $purePDFName -like '*HIS*' -and $purePDFName -like '*Premiumsupport*' ) {
			Move-Item -Path $pdf_filename -Destination $SharePath\HIS-Premiumsupport
			if ( $Error.count -gt 0 ) {
                Start-Sleep -Seconds 5
				Remove-Item -Path $pdf_filename 
				$Error.Clear()
			}
		} elseif ( $purePDFName -like '*IHV*') {
			Move-Item -Path $pdf_filename -Destination $SharePath\IHV
			if ( $Error.count -gt 0 ) {
                Start-Sleep -Seconds 5
				Remove-Item -Path $pdf_filename 
				$Error.Clear()
			} 
		} elseif ( $purePDFName -like '*Inventarsystem*' -and $purePDFName -like '*RRZE*' ) {
			Move-Item -Path $pdf_filename -Destination $SharePath\Inventarsystem-RRZE
			if ( $Error.count -gt 0 ) {
                Start-Sleep -Seconds 5
				Remove-Item -Path $pdf_filename 
				$Error.Clear()
			}
		} elseif ( $purePDFName -like '*KLR*' -and $purePDFName -like '*HPP*' ) {
			Move-Item -Path $pdf_filename -Destination $SharePath\Inventarsystem-RRZE
			if ( $Error.count -gt 0 ) {
                Start-Sleep -Seconds 5
				Remove-Item -Path $pdf_filename 
				$Error.Clear()
			}
		} elseif ( $purePDFName -like '*ODBC*' -and $purePDFName -like '*Config*' ) {
			Move-Item -Path $pdf_filename -Destination $SharePath\KLR-HPP
			if ( $Error.count -gt 0 ) {
                Start-Sleep -Seconds 5
				Remove-Item -Path $pdf_filename 
				$Error.Clear()
			}
		} elseif ( $purePDFName -like '*restore*' -and $purePDFName -like '*rvdb*' ) {
			Move-Item -Path $pdf_filename -Destination $SharePath\restore_rvdb
			if ( $Error.count -gt 0 ) {
                Start-Sleep -Seconds 5
				Remove-Item -Path $pdf_filename 
				$Error.Clear()
			}
		} elseif ( $purePDFName -like '*RV*' -and $purePDFName -like '*Hotline*' ) {
			Move-Item -Path $pdf_filename -Destination $SharePath\RV-Hotline
			if ( $Error.count -gt 0 ) {
                Start-Sleep -Seconds 5
				Remove-Item -Path $pdf_filename 
				$Error.Clear()
			}
		} elseif ( $purePDFName -like '*Wahl*') {
			Move-Item -Path $pdf_filename -Destination $SharePath\Wahl
			if ( $Error.count -gt 0 ) {
                Start-Sleep -Seconds 5
				Remove-Item -Path $pdf_filename 
				$Error.Clear()
			}
		} elseif ( $purePDFName -like '*TAS*' ) {
			Move-Item -Path $pdf_filename -Destination $SharePath\TAS
			if ( $Error.count -gt 0 ) {
                Start-Sleep -Seconds 5
				Remove-Item -Path $pdf_filename 
				$Error.Clear()
			}
		} else {
			cls
			echo "Das Dokument '$purePDFName' konnte nicht eingeordnet werden!"
			Remove-Item -Path $pdf_filename
			$Error.Clear()
			start-sleep -Seconds 2
		}
	}
$word_app.Quit()
Begin(0)
}

# Eigene Pfadangabe

function OwnPath{
	$Error.Clear()
	$word_app = New-Object -ComObject Word.Application

	Get-ChildItem -Path $InputPath -Filter *.doc? | ForEach-Object {

		$document = $word_app.Documents.Open($_.FullName)
		
		$Directory = $_.DirectoryName
		$purePDFName = $_.BaseName
		
		
		$pdf_filename = "$($Directory)\$($purePDFName).pdf"


		$document.SaveAs([ref] $pdf_filename, [ref] 17)

		$document.Close()

		Move-Item -Path $pdf_filename -Destination $DestinationPath
		if ( $Error.count -gt 0 ) {
			Remove-Item -Path $pdf_filename 
			$Error.Clear()
		}
		$word_app.Quit()
		Begin(0)
	}

}

# Zielpfad auswahl

function DestPath {

	cls
	echo '1: RRZE Ressourcenverfahren\Verfahren' '2: Eigene Pfadangabe'  '----------------------'
	$DestinationSwitch = Read-Host -Prompt "Bitte Nummer wählen"

	Switch ( $DestinationSwitch ){
		1 { 
			$DestinationPath = $SharePath 
			RV-Verfahren       
		}
		2 { 
			$DestinationPath = Read-Host -Prompt 'Bitte eigenen Pfad eingeben'
			checkPath(0)
		}
		default {
			Begin(0)
		}
	}	

}


Begin(0)