﻿$documents_path = 'C:\Users\ga39bixy\Desktop\test'   # select deafault path

$word_app = New-Object -ComObject Word.Application # Neue Instanz erzeugen

$i=0 #Laufvariable festlegen
Get-ChildItem -Path $documents_path -Filter *.docx | ForEach-Object { #Namen der Worddokumente bekommen

    If( $i%150 ) { # Debug falls zuviele Worddokumente umgewandelt werden, Instanz neu machen
        $word_app.Quit(); # Instanz beenden
        $word_app = New-Object -ComObject Word.Application # Instanz neu erzeugen
        }

    $document = $word_app.Documents.Open($_.FullName) # genaues Dokument festlegen
    $pdf_filename = "C:\Users\ga39bixy\Desktop\test\test2\$($_.BaseName)_.pdf" # Speicherort und Name festlegen
    $document.SaveAs([ref] $pdf_filename, [ref] 17) # speichern mit PDF Format
    $document.Close() # Worddokument beenden
    $i++ # Laufvariable hochzählen

}
$word_app.Quit() # Instanz beenden