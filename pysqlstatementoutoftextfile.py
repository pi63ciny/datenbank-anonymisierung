#!/usr/bin/env python3

import os
import psycopg2
from psycopg2 import sql
import sys
import argparse

parser = argparse.ArgumentParser()

parser.add_argument('-f','--file', required=True, help="Filelocation")
args = parser.parse_args()
filelocation = args.file


try:
    os.remove("createtables.sql")
except:
    sqlfile = open("createtables.sql", "x")

tablefile = open(filelocation,"r")
lines = tablefile.readlines()
tablefile.close()


for table in lines:
    if 'Table ' in table:
        tablename = table.split("Table ")
        tablenew = tablename[1][:-1]
        sqlfile = open("createtables.sql", "a")
        sqlfile.write("CREATE TABLE " + tablenew + "(id serial PRIMARY KEY);\n")
        sqlfile.close()
    else:
        columnnamefirst = table.split(")")
        columnnamesecond = columnnamefirst[0].split(" ")
        columnnamethird = columnnamesecond[0].split("\t")
        columnname = columnnamethird[1] + (columnnamesecond[1])[:-0] + " " + columnnamesecond[1] + columnnamesecond[2] + ")"
        sqlfile = open("createtables.sql", "a")
        sqlfile.write("ALTER TABLE " + tablenew + " ADD " + columnname + ";\n")
        sqlfile.close()
