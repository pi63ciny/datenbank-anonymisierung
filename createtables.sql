CREATE TABLE db_sachbearbeiter;
ALTER TABLE db_sachbearbeiter ADD telefon NCHAR(20);
ALTER TABLE db_sachbearbeiter ADD name NCHAR(28);
ALTER TABLE db_sachbearbeiter ADD vorname NCHAR(28);
ALTER TABLE db_sachbearbeiter ADD geschlecht NCHAR(1);
ALTER TABLE db_sachbearbeiter ADD email NCHAR(255);
ALTER TABLE db_sachbearbeiter ADD fax NCHAR(20);
ALTER TABLE db_sachbearbeiter ADD institut NCHAR(10);
ALTER TABLE db_sachbearbeiter ADD raumnummer NCHAR(25);
CREATE TABLE pgd;
ALTER TABLE pgd ADD pgd_name NCHAR(35);
ALTER TABLE pgd ADD pgd_vornamen NCHAR(30);
ALTER TABLE pgd ADD pgd_namenbestand_nach NCHAR(28);
ALTER TABLE pgd ADD pgd_wohnort NCHAR(35);
ALTER TABLE pgd ADD pgd_titel NCHAR(4);
ALTER TABLE pgd ADD pgd_strasse NCHAR(50);
ALTER TABLE pgd ADD pgd_alle_vornamen NCHAR(255);
ALTER TABLE pgd ADD pgd_alle_nachnamen NCHAR(255);
ALTER TABLE pgd ADD pgd_namenbestand_nach NCHAR(28);
CREATE TABLE zp;
ALTER TABLE zp ADD anrede NCHAR(20);
ALTER TABLE zp ADD zp_name NCHAR(50);
ALTER TABLE zp ADD str NCHAR(50);
ALTER TABLE zp ADD gebname NCHAR(10);
ALTER TABLE zp ADD plz NCHAR(10);
ALTER TABLE zp ADD ort NCHAR(35);
ALTER TABLE zp ADD land NCHAR(3);
ALTER TABLE zp ADD mbs_konto NCHAR(35);
ALTER TABLE zp ADD kunden_nr NCHAR(20);
ALTER TABLE zp ADD pers NCHAR(25);
ALTER TABLE zp ADD tel NCHAR(20);
ALTER TABLE zp ADD ivs_pers NCHAR(25);
ALTER TABLE zp ADD ivs_tel NCHAR(20);
ALTER TABLE zp ADD email NCHAR(75);
ALTER TABLE zp ADD rufname NCHAR(50);
ALTER TABLE zp ADD steuerid NCHAR(20);
ALTER TABLE zp ADD geschlecht NCHAR(1);
ALTER TABLE zp ADD mbs_bic NCHAR(11);
ALTER TABLE zp ADD mbs_iban NCHAR(35);
ALTER TABLE zp ADD steuerid_persoenlich NCHAR(20);
CREATE TABLE zpa;
ALTER TABLE zpa ADD zp_name1 NCHAR(50);
ALTER TABLE zpa ADD zp_name2 NCHAR(50);
ALTER TABLE zpa ADD plz NCHAR(10);
ALTER TABLE zpa ADD ort NCHAR(35);
ALTER TABLE zpa ADD str NCHAR(50);
ALTER TABLE zpa ADD konto NCHAR(35);
ALTER TABLE zpa ADD blz NCHAR(35);
ALTER TABLE zpa ADD bic NCHAR(11);
ALTER TABLE zpa ADD iban NCHAR(35);
CREATE TABLE huel;
ALTER TABLE huel ADD z_anrede NCHAR(20);
ALTER TABLE huel ADD z_name NCHAR(50);
ALTER TABLE huel ADD z_ort NCHAR(35);
ALTER TABLE huel ADD ao_nr NCHAR(14);
ALTER TABLE huel ADD vorname NCHAR(50);
ALTER TABLE huel ADD z_bic NCHAR(11);
ALTER TABLE huel ADD z_iban NCHAR(35);
ALTER TABLE huel ADD aobef_name NCHAR(20);
ALTER TABLE huel ADD sach_name NCHAR(20);
CREATE TABLE anfr;
ALTER TABLE anfr ADD anfr_anti NCHAR(25);
ALTER TABLE anfr ADD anfr_plz NCHAR(10);
ALTER TABLE anfr ADD anfr_name NCHAR(35);
ALTER TABLE anfr ADD anfr_ort NCHAR(25);
ALTER TABLE anfr ADD anfr_strasse NCHAR(35);
ALTER TABLE anfr ADD anfr_land NCHAR(3);
ALTER TABLE anfr ADD anfr_raumnr NCHAR(8);
ALTER TABLE anfr ADD anfr_zp_kontakt NCHAR(35);
ALTER TABLE anfr ADD anfr_zp_tel NCHAR(20);
ALTER TABLE anfr ADD anfr_zp_fax NCHAR(20);
ALTER TABLE anfr ADD anfr_zp_email NCHAR(75);
ALTER TABLE anfr ADD anfr_inst_kontakt1 NCHAR(35);
ALTER TABLE anfr ADD anfr_inst_tel1 NCHAR(20);
ALTER TABLE anfr ADD anfr_inst_kontakt2 NCHAR(35);
ALTER TABLE anfr ADD anfr_inst_tel2 NCHAR(20);
ALTER TABLE anfr ADD anfr_inst_fax1 NCHAR(20);
ALTER TABLE anfr ADD anfr_inst_email1 NCHAR(75);
ALTER TABLE anfr ADD anfr_inst_fax2 NCHAR(35);
ALTER TABLE anfr ADD anfr_inst_email2 NCHAR(75);
CREATE TABLE ange;
ALTER TABLE ange ADD ange_anti NCHAR(25);
ALTER TABLE ange ADD ange_plz NCHAR(10);
ALTER TABLE ange ADD ange_ort NCHAR(25);
ALTER TABLE ange ADD ange_strasse NCHAR(35);
CREATE TABLE auft;
ALTER TABLE auft ADD auft_name NCHAR(35);
ALTER TABLE auft ADD auft_anti NCHAR(25);
ALTER TABLE auft ADD auft_plz NCHAR(10);
ALTER TABLE auft ADD auft_z_str NCHAR(50);
ALTER TABLE auft ADD auft_z_bic NCHAR(11);
ALTER TABLE auft ADD auft_z_iban NCHAR(35);
ALTER TABLE auft ADD ange_name NCHAR(35);
ALTER TABLE auft ADD ange_land NCHAR(3);
CREATE TABLE mata;
ALTER TABLE mata ADD mata_anti NCHAR(25);
ALTER TABLE mata ADD mata_plz NCHAR(10);
ALTER TABLE mata ADD mata_strasse NCHAR(35);
ALTER TABLE mata ADD mata_land NCHAR(3);
ALTER TABLE mata ADD mata_name NCHAR(35);
ALTER TABLE mata ADD mata_ort NCHAR(25);
ALTER TABLE mata ADD mata_zp_kontakt NCHAR(35);
ALTER TABLE mata ADD mata_zp_tel NCHAR(20);
ALTER TABLE mata ADD mata_zp_fax NCHAR(20);
ALTER TABLE mata ADD mata_zp_email NCHAR(75);
ALTER TABLE mata ADD mata_inst_kontakt1 NCHAR(35);
ALTER TABLE mata ADD mata_inst_tel1 NCHAR(20);
ALTER TABLE mata ADD mata_inst_kontakt2 NCHAR(35);
ALTER TABLE mata ADD mata_inst_tel2 NCHAR(20);
ALTER TABLE mata ADD mata_sbgenehm NCHAR(20);
ALTER TABLE mata ADD mata_email NCHAR(75);
CREATE TABLE mazu;
ALTER TABLE mazu ADD mazu_anti NCHAR(25);
ALTER TABLE mazu ADD mazu_plz NCHAR(10);
ALTER TABLE mazu ADD mazu_strasse NCHAR(35);
ALTER TABLE mazu ADD mazu_land NCHAR(3);
ALTER TABLE mazu ADD mazu_name NCHAR(35);
ALTER TABLE mazu ADD mazu_ort NCHAR(25);
ALTER TABLE mazu ADD mazu_zp_kontakt NCHAR(35);
ALTER TABLE mazu ADD mazu_zp_tel NCHAR(20);
ALTER TABLE mazu ADD mazu_zp_fax NCHAR(20);
ALTER TABLE mazu ADD mazu_zp_email NCHAR(75);
ALTER TABLE mazu ADD mazu_inst_kontakt1 NCHAR(35);
ALTER TABLE mazu ADD mazu_inst_tel1 NCHAR(20);
ALTER TABLE mazu ADD mazu_inst_kontakt2 NCHAR(35);
ALTER TABLE mazu ADD mazu_inst_tel2 NCHAR(20);
ALTER TABLE mazu ADD mazu_inst_fax1 NCHAR(20);
ALTER TABLE mazu ADD mazu_inst_email1 NCHAR(75);
ALTER TABLE mazu ADD mazu_inst_fax2 NCHAR(35);
ALTER TABLE mazu ADD mazu_inst_email2 NCHAR(75);
CREATE TABLE reko;
ALTER TABLE reko ADD reko_anti NCHAR(25);
ALTER TABLE reko ADD reko_plz NCHAR(10);
ALTER TABLE reko ADD reko_ort NCHAR(25);
ALTER TABLE reko ADD reko_strasse NCHAR(35);
ALTER TABLE reko ADD reko_name NCHAR(35);
ALTER TABLE reko ADD reko_land NCHAR(3);
ALTER TABLE reko ADD reko_sbid_bearb INTEGERBearbeitender);
ALTER TABLE reko ADD reko_sbid_aend INTEGERÄndernder);
ALTER TABLE reko ADD reko_zp_kontakt NCHAR(35);
ALTER TABLE reko ADD reko_zp_tel NCHAR(20);
ALTER TABLE reko ADD reko_zp_fax NCHAR(20);
ALTER TABLE reko ADD reko_zp_email NCHAR(75);
ALTER TABLE reko ADD reko_inst_kontakt1 NCHAR(35);
ALTER TABLE reko ADD reko_inst_tel1 NCHAR(20);
ALTER TABLE reko ADD reko_inst_kontakt2 NCHAR(35);
ALTER TABLE reko ADD reko_inst_tel2 NCHAR(20);
ALTER TABLE reko ADD reko_inst_fax1 NCHAR(20);
ALTER TABLE reko ADD reko_inst_email1 NCHAR(75);
ALTER TABLE reko ADD reko_inst_fax2 NCHAR(35);
ALTER TABLE reko ADD reko_inst_email2 NCHAR(75);
CREATE TABLE Druck;
ALTER TABLE Druck ADD vorname NCHAR(50);
ALTER TABLE Druck ADD z_plz NCHAR(10);
ALTER TABLE Druck ADD z_bic NCHAR(11);
ALTER TABLE Druck ADD z_konto NCHAR(35);
ALTER TABLE Druck ADD z_iban NCHAR(35);
ALTER TABLE Druck ADD z_blz NCHAR(35);
ALTER TABLE Druck ADD z_name NCHAR(50);
ALTER TABLE Druck ADD z_ort NCHAR(35);
ALTER TABLE Druck ADD z_plz NCHAR(10);
ALTER TABLE Druck ADD z_str NCHAR(50);
ALTER TABLE Druck ADD sach NCHAR(20);
ALTER TABLE Druck ADD tel NCHAR(20);
CREATE TABLE feststeller;
ALTER TABLE feststeller ADD name NCHAR(50);
ALTER TABLE feststeller ADD vorname NCHAR(30);
CREATE TABLE klinik_imp;
ALTER TABLE klinik_imp ADD vorname NCHAR(15);
ALTER TABLE klinik_imp ADD name NCHAR(20);
CREATE TABLE mbs_import ;
ALTER TABLE mbs_import  ADD z_anrede NCHAR(20);
ALTER TABLE mbs_import  ADD z_name NCHAR(50);
ALTER TABLE mbs_import  ADD z_plz NCHAR(10);
ALTER TABLE mbs_import  ADD z_ort NCHAR(35);
ALTER TABLE mbs_import  ADD z_str NCHAR(50);
ALTER TABLE mbs_import  ADD blz NCHAR(20);
ALTER TABLE mbs_import  ADD konto NCHAR(35);
ALTER TABLE mbs_import  ADD z_vorname NCHAR(50);
CREATE TABLE zpprot;
ALTER TABLE zpprot ADD rufname_a NCHAR(50);
ALTER TABLE zpprot ADD rufname_n NCHAR(50);
ALTER TABLE zpprot ADD steuerid_a NCHAR(20);
ALTER TABLE zpprot ADD steuerid_n NCHAR(20);
ALTER TABLE zpprot ADD bic_a NCHAR(11);
ALTER TABLE zpprot ADD iban_a NCHAR(35);
ALTER TABLE zpprot ADD bic_n NCHAR(11);
ALTER TABLE zpprot ADD iban_n NCHAR(35);
ALTER TABLE zpprot ADD blz_a NCHAR(35);
ALTER TABLE zpprot ADD blz_n NCHAR(35);
ALTER TABLE zpprot ADD plz_a NCHAR(10);
ALTER TABLE zpprot ADD plz_n NCHAR(10);
ALTER TABLE zpprot ADD ort_a NCHAR(35);
ALTER TABLE zpprot ADD ort_n NCHAR(35);
CREATE TABLE ivleihpers ;
ALTER TABLE ivleihpers  ADD ivlp_vorname NCHAR(28);
ALTER TABLE ivleihpers  ADD ivlp_name NCHAR(28);
ALTER TABLE ivleihpers  ADD ivlp_plz NCHAR(10);
ALTER TABLE ivleihpers  ADD ivlp_ort NCHAR(28);
ALTER TABLE ivleihpers  ADD ivlp_strasse NCHAR(50);
ALTER TABLE ivleihpers  ADD ivlp_telnr NCHAR(20);
ALTER TABLE ivleihpers  ADD ivlp_email NCHAR(75);
CREATE TABLE pers;
ALTER TABLE pers ADD pd NCHAR(1);
ALTER TABLE pers ADD name NCHAR(50);
ALTER TABLE pers ADD v_name NCHAR(30);
ALTER TABLE pers ADD kto NCHAR(25);
ALTER TABLE pers ADD blz NCHAR(8);
CREATE TABLE persrueck;
ALTER TABLE persrueck ADD name NCHAR(20);
ALTER TABLE persrueck ADD vorname NCHAR(15);
CREATE TABLE ausl_archiv;
ALTER TABLE ausl_archiv ADD z_name1 NCHAR(50);
ALTER TABLE ausl_archiv ADD z_name2 NCHAR(50);
ALTER TABLE ausl_archiv ADD z_str NCHAR(50);
ALTER TABLE ausl_archiv ADD z_plz NCHAR(10);
ALTER TABLE ausl_archiv ADD z_ort NCHAR(50);
ALTER TABLE ausl_archiv ADD z_blz NCHAR(35);
ALTER TABLE ausl_archiv ADD z_bic NCHAR(11);
