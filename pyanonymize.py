#!/usr/bin/env python3

import psycopg2
from psycopg2 import sql
import sys
from random import randrange
import argparse
from psycopg2.extensions import AsIs
import re


parser = argparse.ArgumentParser()

parser.add_argument('-f','--file', required=True, help="Filelocation")
args = parser.parse_args()
filelocation = args.file

replacefile = open(filelocation,'rt')
replacelines = replacefile.readlines()

linecount = 0
for line in replacelines:
    linecount = linecount +1
    newline = line[:-1]
    if linecount == 1 and newline[0] != '{':
        print("Error! First line must be databaseconnection!")
        exit()
    if (newline[0]) == '{':
        for parameter in newline.split(','):
            if parameter[0] == ' ':
                parameter = parameter[1:]
        
            if "dbname" in parameter:
                dbname = (parameter.split(' '))[1]
                if "}" in dbname:
                    dbname = dbname[:-1]
            if "dbuser" in parameter:
                dbuser = (parameter.split(' '))[1]
                if "}" in dbuser:
                    dbuser = dbuser[:-1]
            elif "dbpass" in parameter:
                dbpass = (parameter.split(' '))[1]
                if "}" in dbpass:
                    dbpass = dbpass[:-1]
            elif "dbhost" in parameter:
                dbhost = (parameter.split(' '))[1]
                if "}" in dbhost:
                    dbhost = dbhost[:-1]
            elif "dbport" in parameter:
                dbport = (parameter.split(' '))[1]
                if "}" in dbport:
                   dbport = dbport[:-1]
    elif (newline[0]) == '[':
        replacetable = (((newline.split(']'))[0]).split('['))[1]
        if "{" in newline:
            replacewithschema = (newline.split('{')[1]).split('}')[0]
    elif newline[0] == '\t' or newline[:4] == '    ':
        try:
            oldtable = (((newline.split(']'))[0]).split('['))[1]
        except:
            print ("ERROR! Syntaxerror in you file [Line: " + str(linecount) + "]!")
            exit()
        else:
            if "(" in oldtable:
                primarykeylist = str(oldtable.split("(")[1]).split(")")[0]
                oldtable = str((oldtable.split("(")[0]))


        if "{" in newline:
            replacementschema = (newline.split('{')[1]).split('}')[0]
        try:
            oldcolumn = ((newline.split(']')[1]).split('['))[1]
        except:
            print ("ERROR! Syntaxerror in your file! [Line: " + str(linecount) + "]!")
            exit()
    else:
        print ("ERROR! Syntaxerror in your file! [Line: " + str(linecount) + "]!")
        exit()


    try:
        dbpass
    except:
        parameterlist = ("dbname=" + dbname + " user=" + dbuser + "")
    else:
        parameterlist = ("dbname=" + dbname + " user=" + dbuser + " password=" + dbpass + "")

    try:
        dbhost
    except:
        error = 0
    else:
        try:
            dbport
        except:
            print("ERROR! You have to specify a port!")
            exit()
        else:
            parameterlist = (parameterlist + " host=" + dbhost + " port=" + dbport)

# Database #

    conn = psycopg2.connect(parameterlist)
    cur = conn.cursor()
    racur = conn.cursor()   
    selcur = conn.cursor()


        
        











# Update original table #

    try:
        selcur.execute(sql.SQL("SELECT %s FROM {}.{}").format(sql.Identifier(replacementschema),sql.Identifier(oldtable)), (AsIs(primarykeylist), )) 
    except:
        error = 0
    else:
        while 1:
            try:
                user = selcur.fetchone()
            except:
                error = 0
            counter2 = 0
 
# random value #

            try:
                racur.execute(sql.SQL("SELECT COUNT (id) FROM {}.{}").format(sql.Identifier(replacewithschema),sql.Identifier(replacetable)))
                anzahl = (racur.fetchall())[0][0]
            except:
                error = 0
            else:
                racur.execute(sql.SQL("SELECT {} FROM {}.{} WHERE id = %s").format(sql.Identifier(replacetable),sql.Identifier(replacewithschema),sql.Identifier(replacetable)),(randrange(anzahl),))
                value = "".join(racur.fetchall()[0])                 
            
            
            
# Update #           
            
            try:
                query = (sql.SQL("UPDATE {}.{} SET {} = {} WHERE {} = {}").format(sql.Identifier(replacementschema), sql.Identifier(oldtable), sql.Identifier(oldcolumn), sql.Literal(value), sql.Identifier(primarykeylist.split(",")[counter2]), sql.Literal(user[counter2])))
            except:
                error = 0
            count = 1
            while len(primarykeylist.split(",")) > (count):
                add = (sql.SQL(" AND {} = {}").format(sql.Identifier(primarykeylist.split(",")[count]), sql.Literal(user[count])))
                query = query + add
                

                cur.execute(query)

                conn.commit()
                count = count + 1
            counter2 = counter2 + 1
            
            try:
                selcur.scroll(0,mode='relative')
            except psycopg2.ProgrammingError:
                break
            



