#!/usr/bin/env python3

import psycopg2
from psycopg2 import sql
import sys
import argparse

parser = argparse.ArgumentParser()

parser.add_argument('-f','--file', required=True, help="Filelocation")
parser.add_argument('-t','--tablename', required=True, help="Tablename")
parser.add_argument('-n','--name', required=True, help="Databasename")
parser.add_argument('-u','--user', required=True, help="Databaseuser")
parser.add_argument('-p','--password', required=False, help="userpassword")
parser.add_argument('-s','--schema', required=True, help="Schema")
parser.add_argument('--host', required=False, help="Hostaddress")
parser.add_argument('--port', required=False, help="Portnumber")

args = parser.parse_args()

filelocation = args.file
tablename = args.tablename
databasename = args.name
databaseuser = args.user
userpassword = args.password
schema = args.schema
dbhost = args.host
dbport = args.port


if userpassword == None:
    connectionstring = ("dbname=" + databasename + " user=" + databaseuser)
else:
    connectionstring = ("dbname=" + databasename + " user=" + databaseuser + " password=" + userpassword)

if dbhost != None:
    if dbport == None:    
        print("ERROR! You have to specify a port!")
        exit()
    else:
        connectionstring = (connectionstring + " host=" + dbhost + " port=" + dbport)



conn = psycopg2.connect(connectionstring)
cur = conn.cursor()




file = open(filelocation, "rt")
lines = file.readlines()
file.close()

try:
    cur.execute(sql.SQL("DROP TABLE {}.{}").format(sql.Identifier(schema),sql.Identifier(tablename)))
except:
    conn.rollback()
finally:
    cur.execute(sql.SQL("CREATE TABLE {}.{} (id serial PRIMARY KEY, {} varchar)").format(sql.Identifier(schema),sql.Identifier(tablename),sql.Identifier(tablename)))

for name in lines:
    cur.execute(sql.SQL("INSERT INTO {}.{} ({}) VALUES (%s)").format(sql.Identifier(schema),sql.Identifier(tablename),sql.Identifier(tablename)),(name[:-1], ))
            
conn.commit()

