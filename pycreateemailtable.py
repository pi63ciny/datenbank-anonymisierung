#!/usr/bin/env python3

import psycopg2
from psycopg2 import sql
import sys
import argparse

parser = argparse.ArgumentParser()

parser.add_argument('-c','--createtable', required=True, help="Tablename of table that gets created")
parser.add_argument('--firsttable', required=True, help="name of first table for informations")
parser.add_argument('--secondtable', required=True, help="name of second table for informations")
parser.add_argument('-n','--name', required=True, help="Databasename")
parser.add_argument('-u','--user', required=True, help="Databaseuser")
parser.add_argument('-p','--password', required=False, help="userpassword")
parser.add_argument('-s','--schema', required=True, help="Schema")
parser.add_argument('--host', required=False, help="Databasehostaddress")
parser.add_argument('--port', required=False, help="Databaseport")

args = parser.parse_args()

tablename = args.createtable
firsttable = args.firsttable
secondtable = args.secondtable
databasename = args.name
databaseuser = args.user
userpassword = args.password
schema = args.schema
dbhost = args.host
dbport = args.port


if userpassword == None:
    connectionstring = ("dbname=" + databasename + " user=" + databaseuser)
else:
    connectionstring = ("dbname=" + databasename + " user=" + databaseuser + " password=" + userpassword)

if dbhost != None:
    if dbport == None:
        print("ERROR! You have to specify a port!")
        exit()
    else:
        connectionstring = (connectionstring + " host=" + dbhost + " port=" + dbport)


conn = psycopg2.connect(connectionstring)
cur = conn.cursor()

def commit():
    conn.commit()
    exit()


###  email  ###
cur.execute(sql.SQL("CREATE TABLE {}.{} (id serial PRIMARY KEY, {} varchar);").format(sql.Identifier(schema),sql.Identifier(tablename),sql.Identifier(tablename)))

cur.execute(sql.SQL("SELECT {} FROM {}.{};").format(sql.Identifier(firsttable),sql.Identifier(schema),sql.Identifier(firsttable)))
firsttable = cur.fetchall()
cur.execute(sql.SQL("SELECT {} FROM {}.{};").format(sql.Identifier(secondtable),sql.Identifier(schema),sql.Identifier(secondtable)))
secondtable = cur.fetchall()
counter = -1
if len(firsttable) >= len(secondtable):
    while len(secondtable) > counter:
        counter = counter + 1
        try:
            singletuple = secondtable[counter]
        except:
            commit()
        secondtablemail =  "".join(singletuple)
        singletuple = firsttable[counter]
        firsttablemail = "".join(singletuple)
        email = (firsttablemail + "." + secondtablemail + "@anonym.de")
        cur.execute(sql.SQL("INSERT INTO {}.{} ({}) VALUES (%s);").format(sql.Identifier(schema),sql.Identifier(tablename),sql.Identifier(tablename)), (email,))

else:
    while len(firsttable) > counter:
        counter = counter + 1
        try:
            singletuple = secondtable[counter]
        except:
            commit()
        secondtablemail =  "".join(singletuple)
        singletuple = firsttable[counter]
        firsttablemail = "".join(singletuple)
        email = (firsttablemail + "." + secondtablemail + "@anonym.de")
        cur.execute(sql.SQL("INSERT INTO {}.{} ({}) VALUES (%s);").format(sql.Identifier(schema),sql.Identifier(tablename),sql.Identifier(tablename)), (email,))


