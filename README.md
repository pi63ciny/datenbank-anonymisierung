#### DOKU ####

**!!! Im Textdateiformat wird die Syntax klar! !!!**

# Tabellen aus Listen erstellen und mit Daten füllen  #


1. Anforderungen

Min. Python version: 3.X  
Module: psycopg2  
User auf der Datenbank erstellen  
Schema für diesen User erstellen  


2. Ausführung  

    1. Listen als Textdatei anlegen   => Liste mit einem Wert pro Zeile!
    2. "pycreatelisttable.py" ausführen     => Tabelle wird erzeugt und mit Datensatz aus der angegebenen Liste gefüllt


# Tabellen anonymisieren #

1. Anforderungen

Min. Python version: 3.X  
Module: psycopg2  
User auf der Datenbank erstellen  
Schema für diesen User erstellen  

2. Benötigte Datei:
    
    - Wählbarer Name  
    
        => Syntax:  
            {dbuser: [Benutzername], dbname: [Datenbankname], (dbpass: [Benutzerpasswort] -> nur wenn Benutzer passwort hat, sonst weg lassen!), dbhost: [host], dbport: [port], key: [primarykey(s)]}  
            [Anonyme Daten Tabelle(PRIMARYKEY)]{Schema}  
            [tab][Tabellenname der zu anonymisierenden Tabelle]{Schema}  
        Bsp.:  
            {dbuser: fsv, dbname: fsv}  
            [vornamen]{anonymschema}  
                [benutzer(vorname,nachname)][vorname]{orginalschema}  
                [angestellte(kennung)][vornamen]  
                [pgd][pgd_vornamen]{public}  
            {dbuser: anonym, dbname: devdb, dbpass: anonympass, dbhost: data.rrze.uni-erlangen.de, dbport: 5432}  
            [nachnamen]{testschema}  
                [angestellte][nachnamen]  

            
            (Bei nichtangabe von einem Schema wird das der vorherigen Zeile verwendet! (Bei der ersten Angabe von der Anonymisierungstabelle und der zu anonymisierenden Tabelle muss jeweils das Schema angegeben werden!))

3. Ausführung

    1. Datei mit Anonymisierungssyntax anlegen
    2. pyanonymize.py ausführen     => Tabellen werden anonymisiert




# Script zum erstellen von Tabellen (wird zum anonymisieren nicht ebnötigt) #


1. Anforderungen

Min. Python version: 3.X  
Module: psycopg2
User auf der Datenbank erstellen  
Schema für diesen User erstellen  



2. Benötigte Dateien:

        Tabellenliste:  
            z. B.:
             table.txt: 
                => Syntax:  
                    - Tabelle [tabellenname]  
                    [tab][spaltenname] [typ] [groeße]  
                 Bsp.:  
                    - Table vornamen
                        vornamenspalte NCHAR (35)  


            (Am Ende kommt immer eine Fehlermeldung, da die Textdatei zuende ist) 
            
3. Ausführung:

    1. Tabellenliste anlegen (im Bsp. "table.txt")
    3. "pysqlstatementsoutoftextfile" ausführen     => "createtables.sql" mit SQL-Statements wird erzeugzt
    3. psql -f createtables.sql ausführen   => Tabellen werden ohne Daten angelegt
    
    

# Script zum erstellen von einer Tabelle mit Email-Adressen (Kann zum anonymisieren verwendet werden ist aber nicht zwingend) #


1. Anforderungen

Min. Python version: 3.X  
Module: psycopg2
User auf der Datenbank erstellen
Schema für diesen User erstellen


2. Ausführung

    "pycreateemailtable" mit passenden parametern ausführen     => Tabelle mit Emails wird erstellt
